import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Movie} from './movie';
import "rxjs/add/operator/map";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http:Http) {
    console.log("skj");
  }

  //rettreiving MovieService
  getMovies()
    {
      return this.http.get('http://ec2-35-173-111-183.compute-1.amazonaws.com:3306/api/movies')
        .map(res => res.json());
    }

  //add MovieS
  addMovies(newMovie)
  {
    console.log(newMovie);
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://ec2-35-173-111-183.compute-1.amazonaws.com:3306/api/movies', JSON.stringify(newMovie),{headers:headers})
      .map(res => res.json());
  }

}
